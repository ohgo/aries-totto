# Undangan Pernikahan Aries - Totto

## Isi

Nama mempelai putra: **Ignatius Aries Kurniawan**  
Nama orang tua mempelai putra: **Felix Soebagio (Tan Tjwan Liong)** & **Irene Lilo Widati (Lie Sian Ien)**  
Nama mempelai putri: **Desak Putu Mahadnyani Pastime**  
Nama orang tua mempelai putri: **Bagus Made Herwin Pastime** & **Putu Trisna Pastime**  

Pawiwahan:
- Hari & tanggal: **Senin, 9 Juli 2018**
- Jam: **11.00 WITA**
- Tempat: **Rumah Lumbung, Jl. By Pass Ngurah Rai  Gg Penyu No. 2, Sanur, Denpasar, Bali**

Pemberkatan:  
- Hari & tanggal: **Sabtu, 14 Juli 2018**
- Jam: **10.00 WITA**
- Tempat: **Gereja Katedral Denpasar, Jl. Tukad Musi No 1 Renon, Denpasar, Bali**

Resepsi:  
- Hari & tanggal: **Sabtu, 14 Juli 2018**
- Jam: **19:00 WITA**
- Tempat: **Sekar Jambu, Jl. Sedap Malam No.99, Kesiman, Denpasar, Bali**
- Google Maps QR:

![QR Sekar Jambu di Google Maps](images/google-map-qr.png)

Alamat Aries & Totto: **Lina Sandells Plan 28 12953 Hägersten, Stockholm, Sweden**  

## Kerangka (contoh)

Menikah:  
...  
Dengan  
...  

Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/i berkenan hadir untuk memberikan doa restu pada pernikahan putra-putri kami  

Rangkaian acara

Pawiwahan:  
Hari, Tanggal, Jam, Tempat  

Pemberkatan:  
Hari, Tanggal, Jam, Tempat  

Resepsi:  
Hari, Tanggal, Jam, Tempat  

Atas kehadiran serta doa restu Bapak/Ibu/Saudara/i kami menyampaikan banyak terima kasih  

Kami yang berbahagia  
...

*Alamat Aries & Totto sebaris kecil di paling bawah*

## Ide Desain

1. Tulisan atau font kaligrafi, warna emas
2. Kertas tebal (board?) ukuran sekitar 15cm x 20cm
3. Gambar denah lokasi?

### Contekan

![](images/contoh-undangan-1.jpg)

![](images/contoh-undangan-2.jpg)
